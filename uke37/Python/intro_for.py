# https://play.kahoot.it/#/k/3424402e-b469-4b48-98b7-270fbc572281 

#coding=utf-8
# For - loopies

# Den enkleste - fra 0 til og uten et heltall


# Den litt mer avanserte: fra et tall til og uten et annet



# Enda mer avansert: Fra et tall, til og uten et annet tall, men hopp n hver gang!
# for i in range(20, 0, -2):
#     print(i)


# Avansertest: Kan man loope tegnene i en streng?


# Avansertestestere: Kan man... uhm...
# Hvis jeg vil loope gjennom mange ulike tall jeg vet på forhånd...
# Er det noe i senere pensum som kan hjelpe meg med det? Pliiiis!!!1! [te]
# Hint til neste år: iterable

# Avansertestest: kan vi... uhm... gå nedover? Telle baklengs? fra bakerste bokstav?

# Avansertestestere: Vi får se om vi har tid til en oppgave eller to om for-løkker.
# Som:
    # spør om og lagre en streng
streng = input("Skriv inn en laaaang streng: ")
    # spør om hvor stort hopp en skal gjøre (n)
hopp = input("Hvor stort hopp en skal gjøre:")
    # skriv ut hver nte bokstav helt til slutten


for i in range(100):
    print(streng[i])

    
    # spør hvor stort hopp du skal gjøre mellom hvert tegn
    # for hvert hopp gjennom strengen skriver du ut om bokstaven er en vokal:
    # bokstav in 'aeiouyæøå'
