# Python Calendar Example

# import calendar module
import calendar

# ask of month and year
year = int(input("year: "))
month = int(input("month: "))

# Displaying the Python calendar
print(calendar.month(year, month))
